import { COLORS, POKE_TYPE_LOCAL } from 'constants/constants';
import { pokeApis } from 'containers/poke-list/services';
import { useQuery } from 'react-query';

import { useFetchApi } from './useFetchApi';

export const useQueryGetDetail = (name: string) => {
    const { data, isLoading, error } = useQuery(`detail-poke-${name}`, () =>
        // eslint-disable-next-line react-hooks/rules-of-hooks
        useFetchApi(pokeApis.getDetailNotification(name))
    );
    const pokemonColorByType = localStorage.getItem(POKE_TYPE_LOCAL);
    let [finalColor] = COLORS;
    if (pokemonColorByType) {
        const parseData = JSON.parse(pokemonColorByType);
        finalColor = parseData[data?.types[0].type.name];
    }
    return { data: finalColor || COLORS[0], isLoading, error };
};

export const useQueryGetPokeType = () => {
    const { data, isLoading, error } = useQuery('poke-type', () =>
        // eslint-disable-next-line react-hooks/rules-of-hooks
        useFetchApi(pokeApis.getPokeType())
    );

    return { data, isLoading, error };
};

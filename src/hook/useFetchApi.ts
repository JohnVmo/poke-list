import type { AxiosRequestConfig } from 'axios';
import axios from 'axios';

export interface IUseFetch {
    url: string;
    method: 'get' | 'delete' | 'post' | 'put' | 'patch';
    isAuth?: boolean;
    payload?: any;
}

export const useFetchApi = async (options: IUseFetch): Promise<any> => {
    try {
        const config: AxiosRequestConfig = {
            baseURL: process.env.REACT_APP_BASE_POKE,
        };

        const response = await axios[options.method](
            '/'.concat(options.url),
            options.payload || config, // the method get & delete wont have data and receive only 2 params -> payload empty -> get config
            config // third param always be config
        );

        return response.data;
    } catch (error: any) {
        return Promise.reject(error);
    }
};

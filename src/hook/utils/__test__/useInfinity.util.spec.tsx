import { NUMBER_ITEM_PER_PAGE } from 'constants/constants';
import { getNextParamsList } from '../useInfinity.util';

describe('useInfinity.util', () => {
    test('should render correct getNextParamsList', () => {
        expect(
            getNextParamsList({
                next: `${process.env.REACT_APP_BASE_POKE}/pokemon?offset=20&limit=${NUMBER_ITEM_PER_PAGE}`,
                count: 1154,
            })
        ).toEqual(2); // 2 page
        expect(
            getNextParamsList({
                next: `${process.env.REACT_APP_BASE_POKE}/pokemon?offset=1154&limit=${NUMBER_ITEM_PER_PAGE}`,
                count: 1154,
            })
        ).toEqual(false);
    });
});

import axios from 'axios';
import { mockAPI, mockAPITrue, mock_result_add_shipment } from 'constants/mock';
import { useFetchApi } from 'hook/useFetchApi';

const mock_fail_result = {
    config: {
        retry: false,
    },
    response: {
        data: { errorCode: '401' },
        status: 401,
    },
};

describe('useFetchAPI', () => {
    test('call use Fetch API success', async () => {
        axios.get = jest
            .fn()
            .mockReturnValue(Promise.resolve(mock_result_add_shipment));
        expect(await useFetchApi(mockAPI.mockFuncCall())).toEqual(
            mock_result_add_shipment.data
        );
    });
});
describe('useFetchAPI fail', () => {
    test('call use Fetch API faild', async () => {
        axios.get = jest.fn().mockReturnValue(Promise.reject(mock_fail_result));
        try {
            await useFetchApi(mockAPITrue.mockFuncCall());
            // eslint-disable-next-line no-empty
        } catch (error) {}
    });
});

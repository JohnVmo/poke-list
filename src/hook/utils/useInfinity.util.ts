import { NUMBER_ITEM_PER_PAGE } from 'constants/constants';

export const getNextParamsList = (lastPage: any) => {
    const totalPage = Math.ceil(Number(lastPage?.count) / NUMBER_ITEM_PER_PAGE);
    const url = new URL(lastPage?.next);
    const offset = url?.searchParams.get('offset');
    const currentPage =
        (Number(offset) + Number(NUMBER_ITEM_PER_PAGE)) / NUMBER_ITEM_PER_PAGE;
    return currentPage < totalPage ? currentPage : false;
};

/* eslint-disable react-hooks/rules-of-hooks */
import { NUMBER_ITEM_PER_PAGE } from 'constants/constants';
import { pokeApis } from 'containers/poke-list/services';
import { useCallback } from 'react';
import { useInfiniteQuery } from 'react-query';

import { useFetchApi } from './useFetchApi';
import { getNextParamsList } from './utils/useInfinity.util';

const commonFetchList = () => {
    return useCallback(
        ({ pageParam = 1 }) =>
            useFetchApi(
                pokeApis.getListPoke(NUMBER_ITEM_PER_PAGE * (pageParam - 1))
            ),
        []
    );
};

export const useInfiniteList = () => {
    const { data, isLoading, hasNextPage, fetchNextPage } = useInfiniteQuery(
        'infinityQuery_list_poke',
        commonFetchList(),
        {
            getNextPageParam: getNextParamsList,
        }
    );
    return {
        data,
        isLoading,
        hasNextPage,
        fetchNextPage,
    };
};

import {
    CustomeImage,
    ItemContainer,
} from 'containers/poke-list/shipment-list.styles';
import { useQueryGetDetail } from 'hook/useQueryGetDetail';
import { useState } from 'react';

const PokeItem = (itemData: any) => {
    const id = itemData.data?.url?.split('/')[6];
    const { data: finalColor, isLoading: isLoadingColor } = useQueryGetDetail(
        itemData?.data?.name
    );
    const [loadingImage, setLoadingImage] = useState(false);

    return (
        <>
            {isLoadingColor ? (
                ''
            ) : (
                <ItemContainer backgroundcolor={finalColor}>
                    <CustomeImage
                        isHidden={loadingImage}
                        src={`${process.env.REACT_APP_BASE_POKE_IMAGE}${id}.png`}
                        alt=""
                        onLoad={() => {
                            setLoadingImage(true);
                        }}
                    />
                    {loadingImage && <p>{itemData?.data?.name}</p>}
                </ItemContainer>
            )}
        </>
    );
};

export default PokeItem;

export const onScrollBottomEvent = (
    funtionTrigger: any,
    checkingCondition?: boolean
) => {
    window.onscroll = () => {
        const isReach =
            window.innerHeight + window.scrollY - document.body.scrollHeight;
        if (isReach > -1 && isReach < 1) {
            // eslint-disable-next-line no-unused-expressions
            checkingCondition && funtionTrigger();
        }
    };
};

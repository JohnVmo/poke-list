import { cleanup, render, screen } from '@testing-library/react';
import { createMemoryHistory } from 'history';
import { QueryClient, QueryClientProvider } from 'react-query';
import { Router } from 'react-router-dom';
import App from 'App';

const history = createMemoryHistory();
describe('Test action api', () => {
    afterEach(() => {
        cleanup();
        jest.resetAllMocks();
    });

    test('Able to render created pokeList', () => {
        setUp();
        expect(screen.queryByTestId('pokeList-test')).not.toBeNull();
    });
});

const setUp = () => {
    render(
        <QueryClientProvider client={new QueryClient()}>
            <Router history={history}>
                <App />
            </Router>
        </QueryClientProvider>
    );
};

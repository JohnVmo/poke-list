import './App.css';

import { createBrowserHistory } from 'history';
import React from 'react';
import { Route, Router, Switch } from 'react-router-dom';
import PokeList from 'containers/poke-list/poke-list';

export const history = createBrowserHistory();

function App(): React.ReactElement {
    return (
        <Router history={history}>
            <Switch>
                <Route path="/" exact component={PokeList} />
            </Switch>
        </Router>
    );
}

export default App;

export const NUMBER_ITEM_PER_PAGE = 20;

export const COLORS = [
    '#a1a1a1',
    '#bbd4e9',
    '#36c7bd',
    '#73c2da',
    '#7e5231',
    '#474642',
    '#7ed364',
    '#bb3436',
    '#c05846',
    '#b18260',
    '#436e7a',
    '#376370',
    '#f8d56b',
    '#da3748',
    '#6dadab',
    '#a8ebad',
    '#f8de86',
    '#5c5496',
    '#d5e5ff',
    '#b3909c',
];

export const POKE_TYPE_LOCAL = 'POKE_TYPE';

import type { IUseFetch } from 'hook/useFetchApi';

export const mockAPI = {
    mockFuncCall: (): IUseFetch => ({
        url: `mock`,
        method: 'get',
        isAuth: false,
    }),
};

export const mockAPITrue = {
    mockFuncCall: (): IUseFetch => ({
        url: `mock`,
        method: 'get',
        isAuth: true,
    }),
};

export const mock_result_add_shipment = {
    data: {
        createdAt: '2021-12-09T13:47:55.823Z',
        shipmentId: 'TESTFARLEY0003',
        updatedAt: '2021-12-09T13:47:55.823Z',
        userId: 'fc0b83fa-a2ca-4e2b-9e8d-785410f5bf02',
        __v: 0,
        _id: '61b2090b0dd047205bee7b86',
    },
    message: 'CREATED',
    statusCode: 201,
};

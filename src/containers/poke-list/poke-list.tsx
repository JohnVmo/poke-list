import PokeItem from 'components/pokeIItem/pokeItem';
import { onScrollBottomEvent } from 'components/utils';
import { COLORS, POKE_TYPE_LOCAL } from 'constants/constants';
import { useInfiniteList } from 'hook/useInfinity';
import { useQueryGetPokeType } from 'hook/useQueryGetDetail';
import { useEffect } from 'react';

import {
    CenterContainer,
    OuterContainer,
    PokeListContainer,
} from './shipment-list.styles';

const PokeList = () => {
    const { data, isLoading, hasNextPage, fetchNextPage } = useInfiniteList();
    const { data: pokeType, isLoading: isLoadingPokeType } =
        useQueryGetPokeType();
    onScrollBottomEvent(fetchNextPage, hasNextPage);

    useEffect(() => {
        if (!isLoadingPokeType) {
            let typeColor = {};
            pokeType.results?.forEach((item: any, index: any) => {
                typeColor = {
                    ...typeColor,
                    [item.name]: COLORS[index],
                };
            });
            localStorage.setItem(POKE_TYPE_LOCAL, JSON.stringify(typeColor));
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isLoadingPokeType]);

    return (
        <OuterContainer data-testid="pokeList-test">
            <CenterContainer>
                {!isLoading && (
                    <PokeListContainer>
                        {data?.pages.map((page) =>
                            page.results.map((item: any, index: number) => (
                                <PokeItem key={index} data={item} />
                            ))
                        )}
                    </PokeListContainer>
                )}
            </CenterContainer>
        </OuterContainer>
    );
};

export default PokeList;

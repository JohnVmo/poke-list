import { cleanup, render, screen } from '@testing-library/react';
import { createMemoryHistory } from 'history';
import { QueryClient, QueryClientProvider } from 'react-query';
import { Router } from 'react-router-dom';
import PokeList from '../poke-list';
import * as useInfinity from 'hook/useInfinity';
import * as useQuery from 'hook/useQueryGetDetail';
import { POKE_TYPE_LOCAL } from 'constants/constants';

const history = createMemoryHistory();
export const mockTypeList =
    '{"normal":"#a1a1a1","fighting":"#bbd4e9","flying":"#36c7bd","poison":"#73c2da","ground":"#7e5231","rock":"#474642","bug":"#7ed364","ghost":"#bb3436","steel":"#c05846","fire":"#b18260","water":"#436e7a","grass":"#376370","electric":"#f8d56b","psychic":"#da3748","ice":"#6dadab","dragon":"#a8ebad","dark":"#f8de86","fairy":"#5c5496","unknown":"#d5e5ff","shadow":"#b3909c"}';
describe('Test action api', () => {
    afterEach(() => {
        cleanup();
        jest.resetAllMocks();
    });
    test('Able to render created pokeList', () => {
        setUp();
        localStorage.setItem(POKE_TYPE_LOCAL, JSON.stringify(mockTypeList));
        expect(screen.queryByTestId('pokeList-test')).not.toBeNull();
    });

    test('snapshot test', () => {
        jest.spyOn(useInfinity, 'useInfiniteList').mockReturnValue({
            data: {
                pages: [
                    {
                        results: [
                            {
                                name: 'bulbasasur',
                                url: `${process.env.POKE_BASE_URL}/pokemon/1/`,
                            },
                        ],
                    },
                ],
                pageParams: [],
            },
            isLoading: false,
            hasNextPage: false,
            fetchNextPage: jest.fn(),
        });
        jest.spyOn(useQuery, 'useQueryGetPokeType').mockReturnValue({
            isLoading: false,
            data: {
                results: [
                    {
                        name: 'normal',
                        url: `${process.env.POKE_BASE_URL}/type/1/`,
                    },
                ],
            },
            error: undefined,
        });
        const { asFragment } = render(
            <QueryClientProvider client={new QueryClient()}>
                <Router history={history}>
                    <PokeList />
                </Router>
            </QueryClientProvider>
        );

        expect(asFragment()).toMatchSnapshot();
    });
});

const setUp = () => {
    render(
        <QueryClientProvider client={new QueryClient()}>
            <Router history={history}>
                <PokeList />
            </Router>
        </QueryClientProvider>
    );
};

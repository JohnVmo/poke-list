import styled, { css } from 'styled-components';

export const OuterContainer = styled.div`
    margin: auto;
    @media screen and (min-width: 600px) {
        width: 60%;
    }
`;

export const CenterContainer = styled.div`
    margin: 0.5em;
`;

export const PokeListContainer = styled.div`
    display: grid;
    grid-gap: 0.5em;
    grid-template-columns: repeat(auto-fit, minmax(145px, 1fr));
    @media screen and (min-width: 600px) {
        grid-template-columns: repeat(auto-fit, minmax(200px, 1fr));
    }
`;

export const ItemContainer = styled.div`
    display: flex;
    flex-direction: column;
    border-radius: 10px;
    color: white;
    align-items: center;
    justify-content: center;
    padding: 20px;
    ${(props: { backgroundcolor: string }) =>
        css`
            background-color: ${props.backgroundcolor};
        `}
    img {
        width: 100%;
    }
    p {
        margin: 0px;
    }
`;

export const CustomeImage = styled.img`
    ${(props: { isHidden: boolean }) =>
        css`
            visibility: ${props.isHidden ? 'visible' : 'hidden'};
        `}
`;

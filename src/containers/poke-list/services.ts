import { NUMBER_ITEM_PER_PAGE } from 'constants/constants';
import type { IUseFetch } from 'hook/useFetchApi';

export const pokeApis = {
    getListPoke: (offset?: any): IUseFetch => {
        return {
            url: `pokemon?limit=${NUMBER_ITEM_PER_PAGE}&offset=${
                offset < 0 ? 0 : offset
            }`,
            method: 'get',
        };
    },
    getDetailNotification: (name: string): IUseFetch => {
        return {
            url: `pokemon/${name}`,
            method: 'get',
        };
    },
    getPokeType: (): IUseFetch => {
        return {
            url: `type`,
            method: 'get',
        };
    },
};
